import {Component, OnInit} from '@angular/core';
import{ Location } from '@angular/common';
import { Router } from '@angular/router';
import { StorageService } from './Services/storage/storage.service';
import { AuthService } from './Services/auth/auth.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'SdFoodx';
  show: boolean = false;
  constructor(
    location:Location,
    private router: Router,
    private storageService: StorageService,
    private authService: AuthService
  ){
    router.events.subscribe((val)=>{
      this.show = location.path() != '' ? true : false;
    });
  }
  ngOnInit(){
    // Redirect user if cookie is valid
    if (this.authService.isAuthenticated() && window.location.pathname == '/') {
      const role = this.storageService.getCookie('role');
      if (
        role == 'canteen'
        
      ) {
        this.router.navigate(['/managefood']);
      } else if (role == 'employee') {
        this.router.navigate(['/myorders']);
      }
    }
  }
  
}
