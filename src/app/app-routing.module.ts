import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ManageordersComponent } from './Pages/manageorders/manageorders.component';
import { ManagefoodComponent } from './Pages/managefood/managefood.component';
import { SchedulefoodComponent } from './Pages/schedulefood/schedulefood.component';
import { SpecialRequestFormComponent } from './Pages/specialRequestForm/specialRequestForm.component';
import { DailyordersComponent } from './Pages/dailyorders/dailyorders.component';
import { LoginComponent } from './Pages/login/login.component';
import { OrderfoodComponent } from './Pages/orderfood/orderfood.component';
import { MyordersComponent } from './Pages/myorders/myorders.component';
import { ManagerequestComponent } from './Pages/manageRequest/managerequest.component';
import { SpecialrequestTableComponent } from './Pages/specialrequest-table/specialrequest-table.component';
import { AuthGuard } from './Services/auth/auth-guard.service';
import { ReportComponent } from './Pages/Report/report/report.component';

const routes: Routes = [
  {
    path: '',
    component: LoginComponent,
  },
  {
    path: 'managefood',
    component: ManagefoodComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'dailyorders',
    component: DailyordersComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'manageorders',
    component: ManageordersComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'myorders',
    component: MyordersComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'managerequests',
    component: ManagerequestComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'schedulefood',
    component: SchedulefoodComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'orderfood',
    component: OrderfoodComponent,
    canActivate: [AuthGuard],
  },

  {
    path: 'report',
    component: ReportComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'myspecialrequest',
    component: SpecialrequestTableComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'newspecialrequest',
    component: SpecialRequestFormComponent,
    canActivate: [AuthGuard],
  },

  { path: '**', redirectTo: '', pathMatch: 'full' },
];

@NgModule({
  imports: [RouterModule.forRoot([...routes])],
  exports: [RouterModule],
})
export class AppRoutingModule {}
