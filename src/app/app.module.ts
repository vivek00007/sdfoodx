import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { AddfoodComponent } from './Pages/addfood/addfood.component';
import { ManageordersComponent } from './Pages/manageorders/manageorders.component';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { OrdersService } from './Services/orders/orders.service';
import { MatCardModule } from '@angular/material/card';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatButtonModule } from '@angular/material/button';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatSortModule } from '@angular/material/sort';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatInputModule } from '@angular/material/input';
import { MatIconModule } from '@angular/material/icon';
import { MatSelectModule } from '@angular/material/select';
import { MatTableModule } from '@angular/material/table';
import { MatRadioModule } from '@angular/material/radio';
import { ManagefoodComponent } from './Pages/managefood/managefood.component';
import { DailyordersComponent } from './Pages/dailyorders/dailyorders.component';
import { FoodService } from './Services/food/food.service';
import { SchedulesService } from './Services/schedules/schedules.service';
import { FormsModule } from '@angular/forms';
import { FooterComponent } from './shared/footer/footer.component';
import { CustomMaterialModule } from './material.module';
import { ReactiveFormsModule } from '@angular/forms';
import { MDBBootstrapModule } from 'angular-bootstrap-md';
import { SweetAlert2Module } from '@sweetalert2/ngx-sweetalert2';
import { SpinnerComponent } from './shared/spinner/spinner.component';
import { NavigationbarComponent } from './shared/navigationbar/navigationbar.component';
import { SchedulefoodComponent } from './Pages/schedulefood/schedulefood.component';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { SpecialRequestFormComponent } from './Pages/specialRequestForm/specialRequestForm.component';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { DateInputsModule } from '@progress/kendo-angular-dateinputs';
import { LoginComponent } from './Pages/login/login.component';
import { AuthService } from './Services/auth/auth.service';
import { StorageService } from './Services/storage/storage.service';
import { MyHttpInterceptorService } from './Services/my-HttpInterceptor.service';
import { AuthGuard } from './Services/auth/auth-guard.service';
import { OrderfoodComponent } from './Pages/orderfood/orderfood.component';
import { FoodcardComponent } from './Pages/orderfood/foodcard/foodcard.component';
import { MycartComponent } from './Pages/orderfood/mycart/mycart.component';
import { MyordersComponent } from './Pages/myorders/myorders.component';
import { OrdercardComponent } from './Pages/myorders/ordercard/ordercard.component';
import { ManagerequestComponent } from './Pages/manageRequest/managerequest.component';
import { SpecialService } from './Services/specialRequest/special.service';
import { SpecialrequestTableComponent } from './Pages/specialrequest-table/specialrequest-table.component';
import { ManagecategoryComponent } from './Pages/managecategory/managecategory.component';
import { ReportComponent } from './Pages/Report/report/report.component';
import { MatButtonToggleModule } from '@angular/material/button-toggle';

@NgModule({
  declarations: [
    AppComponent,
    AddfoodComponent,
    ManageordersComponent,
    ManagefoodComponent,
    FooterComponent,
    SpinnerComponent,
    DailyordersComponent,
    NavigationbarComponent,
    SchedulefoodComponent,
    SpecialRequestFormComponent,
    DailyordersComponent,
    LoginComponent,
    OrderfoodComponent,
    FoodcardComponent,
    MycartComponent,
    MyordersComponent,
    OrdercardComponent,
    ManagerequestComponent,
    SpecialrequestTableComponent,
    ManagecategoryComponent,
    ReportComponent,
  ],
  entryComponents: [],

  imports: [
    MDBBootstrapModule.forRoot(),
    FormsModule,
    DragDropModule,
    ReactiveFormsModule,
    HttpClientModule,
    BrowserModule,
    AppRoutingModule,
    MatSelectModule,
    MatAutocompleteModule,
    MatButtonToggleModule,
    MatSortModule,
    MatIconModule,
    MatInputModule,
    MatPaginatorModule,
    MatCardModule,
    MatProgressSpinnerModule,
    MatFormFieldModule,
    MatGridListModule,
    MatButtonModule,
    MatTableModule,
    MatRadioModule,
    CustomMaterialModule,
    SweetAlert2Module,
    BrowserAnimationsModule,
    MatDatepickerModule,
    DateInputsModule,
  ],
  exports: [
    FormsModule,
    ReactiveFormsModule,
    MatSelectModule,
    MatAutocompleteModule,
    MatSortModule,
    MatIconModule,
    MatInputModule,
    MatPaginatorModule,
    MatCardModule,
    MatProgressSpinnerModule,
    MatFormFieldModule,
    MatGridListModule,
    MatButtonModule,
    MatTableModule,
    MatRadioModule,
    DateInputsModule,
  ],
  providers: [
    OrdersService,
    FoodService,
    SchedulesService,
    AuthService,
    StorageService,
    SpecialService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: MyHttpInterceptorService,
      multi: true,
    },
    AuthGuard,
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
