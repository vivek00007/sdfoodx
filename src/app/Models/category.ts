export interface CategoryData {
    category_id: number;
    category_name: string;
}
