export interface MyOrderData {
    food_name: string;
    comment: string;
    quantity: number;
    price: number;
    orderDate: string;
    orderId: number;
    status: number;
    food_image: string;
    food_id: number; //needed for the patch later
}
