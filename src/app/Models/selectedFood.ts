export interface SelectedFood {
    totalPrice: number;
    quantityBought: number;
    foodId: number;
    foodName: string;
    orderDate: string;
}
