export class RequestData {
  emp_name: string;
  emp_id: number;
  special_id: number;
  description: string;
  comment: string;
  request_date: Date;
  total_price: number;
  status: number;
}