export interface AllOrderData {
    emp_name: string;
    food_name: string;
    category_id: number;
    quantity: number;
    totalprice: number;
    status: number;
    comment: string;
    orderDate: string; // needed for the patch
    order_id: number; // needed for the patch
    food_id: number; // needed for the patch
}
