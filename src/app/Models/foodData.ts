export interface FoodData {
    food_id: number;
    food_name: string;
    food_description: string;
    food_price: number;
    food_image: string;
    food_limit: number;
    isVeg: boolean;
    food_rating: number;
    category_name: string;
    category_id: number;
    day: string;
    day_id: number;
}