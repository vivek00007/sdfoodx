import { FoodItem } from './food_item';
export class Column {
    constructor(public name: string, public tasks: FoodItem[]) {}
}