import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root',
})
export class OrdersService {
  constructor(private http: HttpClient) { }

  getAllOrders(): Observable<any> {
    return this.http.get(`${environment.apiUrl}/Orders`);
  }

  addOrders(orders) {
    const body = orders;
    return this.http.post(`${environment.apiUrl}/Orders`, JSON.stringify(body));
  }

  changeStatus(body) {
    console.log(JSON.stringify(body));
    return this.http.patch(
      `${environment.apiUrl}/Orders`,
      JSON.stringify(body)
    );
  }

  updateOrder(orders) {
    const body = orders;
    return this.http.patch(
      `${environment.apiUrl}/Orders`,
      JSON.stringify(body)
    );
  }

  deleteOrder(orders) {
    const options = {
      headers: new HttpHeaders({
        // 'Content-Type': 'application/json',
      }),
      body: (orders),
    };
    return this.http.delete(
      `${environment.apiUrl}/Orders`,
      options
    );
  }
}
