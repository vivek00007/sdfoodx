import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient, HttpParams } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { FoodData } from 'src/app/Models/foodData';

@Injectable({
  providedIn: 'root'
})
export class FoodService {

  constructor(private http: HttpClient) { }

  getAllFoods(): Observable<any> {
    return this.http.get(`${environment.apiUrl}/FoodItems`);
  }

  addFood(bookings) {
    const body = bookings;
    return this.http.post(
      `${environment.apiUrl}/FoodItems`,
      JSON.stringify(body)
    );
  }

  deleteFood(id: number) {
    return this.http.delete(`${environment.apiUrl}/FoodItems/${id}`);
  }

  updateFood(newFood) {
    const body = newFood;
    return this.http.put(
      `${environment.apiUrl}/FoodItems`,
      JSON.stringify(body)
    );
  }
}
