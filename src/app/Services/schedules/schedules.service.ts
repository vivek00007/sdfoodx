import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Schedule } from 'src/app/models/Schedule.model';

@Injectable({
  providedIn: 'root'
})
export class SchedulesService {

  constructor(private http: HttpClient) { }

  getAllSchedules(): Observable<any> {
    return this.http.get(`${environment.apiUrl}/schedules`);
  }
  addSchedules(schedules:Schedule[]): Observable<any>{
    return this.http.post(`${environment.apiUrl}/schedules`,schedules);
  }
  deleteSchedules(schedules:Schedule[]): Observable<any>{
    const options = {
      headers: new HttpHeaders({
        // 'Content-Type': 'application/json',
      }),
      body: (schedules),
    };
    return this.http.delete(`${environment.apiUrl}/schedules`,options);
  }
}
