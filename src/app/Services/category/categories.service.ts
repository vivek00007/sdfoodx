import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient, HttpParams } from '@angular/common/http';
import { environment } from '../../../environments/environment';
// import { StorageService } from '../../Services/storage/storage.service';

@Injectable({
  providedIn: 'root'
})
export class CategoriesService {

  constructor(private http: HttpClient) { }

  getAllCategories(): Observable<any> {
    return this.http.get(`${environment.apiUrl}/Categories`);
  }

  deleteCategory(id: number) {
    return this.http.delete(`${environment.apiUrl}/Categories/${id}`);
  }

  addCategory(categories) {
    const body = categories;
    return this.http.post(
      `${environment.apiUrl}/Categories`,
      JSON.stringify(body)
    );
  }

}
