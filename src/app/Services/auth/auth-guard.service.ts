import { Injectable } from '@angular/core';
import {
  CanActivate,
  CanActivateChild,
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
  Router,
} from '@angular/router';
import { AuthService } from './auth.service';
import { StorageService } from '../storage/storage.service';

@Injectable()
export class AuthGuard implements CanActivate, CanActivateChild {
  constructor(
    private authService: AuthService,
    private router: Router,
    private storageService: StorageService
  ) {}

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): boolean {
    const next_path = next.url[0].path;
    if (this.authService.isAuthenticated()) {
      const role = this.storageService.getCookie('role');
console.log(role)


      if (next_path == 'myorders') {
        if (role == 'employee') {
          return true;
        } else {
          window.location.href = '/';
          return false;
        }
      } else if (next_path == 'orderfood') {
        if (role == 'employee') {
          return true;
        } else {
          window.location.href = '/';
          return false;
        }
      } else if (next_path == 'newspecialrequest') {
        if (role == 'employee') {
          return true;
        } else {
          window.location.href = '/';
          return false;
        }
      } else if (next_path == 'myspecialrequest') {
        if (role == 'employee') {
          return true;
        } else {
          window.location.href = '/';
          return false;
        }
      } else if (next_path == 'managefood') {
        if (role == 'canteen') {
          return true;
        } else {
          window.location.href = '/';
          return false;
        }
      } else if (next_path == 'schedulefood') {
        if ( role == 'canteen') {
          return true;
        } else {
          window.location.href = '/';
          return false;
        }
      }
      else if (next_path == 'managerequests') {
        if ( role == 'canteen') {
          return true;
        } else {
          window.location.href = '/';
          return false;
        }
      }
      else if (next_path == 'dailyorders') {
        if ( role == 'canteen') {
          return true;
        } else {
          window.location.href = '/';
          return false;
        }
      }
      else if (next_path == 'manageorders') {
        if ( role == 'canteen') {
          return true;
        } else {
          window.location.href = '/';
          return false;
        }
      }
      return true;
    } else {
      localStorage.setItem('path', next_path);
      this.router.navigate(['/']);
      return false;
    }
  }

  canActivateChild(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): boolean {
    if (this.authService.isAuthenticated()) {
      const role = this.storageService.getCookie('role');
      console.log(next.url);
      return true;
    }
    this.router.navigate(['/']);
    return false;
    // window.location.href = '/';
  }
}
