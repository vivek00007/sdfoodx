import { RequestData } from './../../Models/RequestData';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { formatDate } from '@angular/common';
import { environment } from 'src/environments/environment';
@Injectable({
  providedIn: 'root',
})
export class SpecialService {
  constructor(private http: HttpClient) { }

  submitRequest(
    description: string,
    comment: string,
    date: Date
  ): Observable<any> {
    const body = {
      description: description,
      comment: comment,
      request_date: formatDate(date, 'yyyy/MM/dd', 'en'),
    };
    console.log(body);
    return this.http.post(`${environment.apiUrl}/SpecialRequests`, body);
  }

  getRequests(): Observable<any> {
    return this.http.get(`${environment.apiUrl}/SpecialRequests`);
  }
  deleteOrder(orders) {
    const options = {
      headers: new HttpHeaders({
        // 'Content-Type': 'application/json',
      }),
      body: (orders),
    };
    return this.http.delete(
      `${environment.apiUrl}/Orders`,
      options
    );
  }
  deleteRequests(special_id: number): Observable<any> {
    const options = {
      headers: new HttpHeaders({
      }),
    };
    return this.http.delete(`${environment.apiUrl}/SpecialRequests/${special_id}`, options);
  }

  updateRequests(newRequest): Observable<any> {
    const body = newRequest;
    return this.http.put(`${environment.apiUrl}/SpecialRequests`, JSON.stringify(body));
  }

  patchRequests(newRequest: RequestData): Observable<any> {
    const body = newRequest;
    return this.http.patch(`${environment.apiUrl}/SpecialRequests`, JSON.stringify(body));
  }

  changeStatus(body): Observable<any> {
    console.log(JSON.stringify(body));
    return this.http.patch(`${environment.apiUrl}/SpecialRequests`, JSON.stringify(body));
  }
}