import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { getTranslationDeclStmts } from '@angular/compiler/src/render3/view/template';
import { environment } from 'src/environments/environment';
@Injectable({
  providedIn: 'root',
})
export class ReportService {
  constructor(private http: HttpClient) {}

  getStats(dateFrom, dateTo) {
    const url = `${environment.apiUrl}/report?dateFrom=${dateFrom}&dateTo=${dateTo}`;
    return this.http.get(url);
  }
}
