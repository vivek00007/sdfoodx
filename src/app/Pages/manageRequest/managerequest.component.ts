import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import { ActivatedRoute, Router } from '@angular/router';
import { RequestData } from '../../Models/RequestData';
import Swal from 'sweetalert2';
import { SpecialService } from 'src/app/Services/specialRequest/special.service';

@Component({
  selector: 'app-managerequest',
  templateUrl: './managerequest.component.html',
  styleUrls: ['./managerequest.component.css']
})
export class ManagerequestComponent implements OnInit {

  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;

  displayedColumns: string[] = [
    'emp_name',
    'description',
    'comment',
    'request_date',
    'total_price',
    'edit',
    'actions',
  ];
  //Filters:
  search: string = '';
  showClearButton = false;
  _loading: boolean = true;
  _loadingMessage = 'Loading Special Requests';
  allRequest: Array<RequestData>;
  dataSource: MatTableDataSource<RequestData>;

  // variables for inline editing of food data
  oldRow = {} as RequestData;
  newRow = {} as RequestData;
  editDisabled: boolean;

  constructor(
    private RS: SpecialService,
    private route: ActivatedRoute,
    private router: Router,
  ) { }

  ngOnInit() {
    this.fetchRequest();
  }

  fetchRequest(): void {
    this.RS.getRequests().subscribe(
      (data: Array<RequestData>) => {
        console.log(data);
        this.allRequest = data;
        this.dataSource = new MatTableDataSource(data);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
        this._loading = false;
      },
      (err) => console.log(err)
    );
  }

  filterRequest(): void {
    let filteredRequest: RequestData[] = [...this.allRequest];
    this.dataSource = new MatTableDataSource(filteredRequest);
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  applyFilter(event: Event): void {
    const filterValue: string = (event.target as HTMLInputElement).value;
    this.search = filterValue;
    this.showClearButton = true;
    this.dataSource.filter = filterValue.trim().toLowerCase();
    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  clearFilters(): void {
    this.showClearButton = false;
    this.dataSource = new MatTableDataSource(this.allRequest);
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
    this.router.navigate(['/managerequests']);
  }

  handleEdit(oldRequest: any): void {
    console.log(oldRequest);
    this.newRow = oldRequest && oldRequest.special_id ? oldRequest : {};
    this.oldRow = { ...this.newRow };
  }

  confirmEdit(): void {
    if (this.newRow.total_price < 0) {
      Swal.fire(
        'Price is not valid!',
        'Please input a valid price.',
        'warning'
      );
    }
    else {
      this.editDisabled = true;
      // this.RS.updateRequests(this.newRow)
      this.RS.patchRequests(this.newRow)
        .subscribe((data: any) => {
          this.newRow = {} as RequestData;
          this.editDisabled = false;
          this.clearFilters();
          Swal.fire(
            'Success!',
            'Details modified.',
            'success'
          );
        }, err => {
          Swal.fire(
            'Failure!',
            'Details were not modified.',
            'error'
          );
          this.editDisabled = false;
          this.cancelEdit();
        });
    }
  }

  cancelEdit(): void {
    this.newRow = {} as RequestData;
    const index = this.allRequest.findIndex(item => item.emp_id === this.oldRow.emp_id);
    this.allRequest.splice(index, 1, this.oldRow);
    this.dataSource = new MatTableDataSource(this.allRequest);
    this.editDisabled = false;
    this.clearFilters();
  }

  changeStatus(special_id: number, status: number): void {
    const body =
    {
      special_id: special_id,
      status: status,
    };
    console.log('mo body');
    console.log(body);
    this.RS.changeStatus(body).subscribe(
      (data: any) => {
        this.fetchRequest();
        this.clearFilters();
        Swal.fire(
          'Success!',
          'Status Changed Successfully!',
          'success'
        );
      },
      (err) => {
        console.log(err);
        Swal.fire(
          'Failure!',
          'Status Changed Failed!',
          'error'
        );
      }
    );
  }
}