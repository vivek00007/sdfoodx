import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import { SpecialService } from 'src/app/Services/specialRequest/special.service';
import { RequestData } from '../../Models/RequestData';
import { Router } from '@angular/router';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-specialrequest-table',
  templateUrl: './specialrequest-table.component.html',
  styleUrls: ['./specialrequest-table.component.css'],
})
export class SpecialrequestTableComponent implements OnInit {
  displayedColumns: string[] = [
    'request_date',
    'comment',
    'description',
    'total_price',
    'status',
  ];
  _loading: boolean = true;
  _loadingMessage = 'Loading your Special Requests';

  dataSource: MatTableDataSource<RequestData>;
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  constructor(private specialService: SpecialService, private router: Router) { }

  ngOnInit(): void {
    this.fetchSpecialRequests();
  }

  fetchSpecialRequests(): void {
    this.specialService.getRequests().subscribe(
      (data: Array<RequestData>) => {
        this._loading = false;
        this.dataSource = new MatTableDataSource(data);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      },
      (err) => {
        console.log(err);
      }
    );
  }

  ChangeViewDateFormat(d: Date): any {
    const d2: Date = new Date(d);
    const dd = String(d2.getDate()).padStart(2, '0');
    const mm = String(d2.getMonth() + 1).padStart(2, '0'); // January is 0!
    const yyyy = d2.getFullYear();
    return dd + '-' + mm + '-' + yyyy;
  }
  getStatus(status: number): string {
    if (status == 0) return 'pending';
    if (status == 1) return 'approved';
    if (status == 2) return 'rejected';
  }

  getStatusNumber(status: string): number {
    if (status == 'pending') return 0;
    if (status == 'approved') return 1;
    if (status == 'rejected') return 2;
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  cancelRequest(specialId: number): void {
    console.log(specialId);
    this.specialService.deleteRequests(specialId).subscribe(
      (data) => {
        Swal.fire({
          title: 'Are you sure you want to cancel this request?',
          icon: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'Yes',
        }).then((result) => {
          if (result.value) {
            Swal.fire('Success!', 'You have canceled your request.', 'success').then(
              () => {
                this.fetchSpecialRequests();
              }
            );
          }

        });
      },
      (err) => {
        console.log(err);
      }
    );
  }
}
