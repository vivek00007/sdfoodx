import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SpecialrequestTableComponent } from './specialrequest-table.component';

describe('SpecialrequestTableComponent', () => {
  let component: SpecialrequestTableComponent;
  let fixture: ComponentFixture<SpecialrequestTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SpecialrequestTableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SpecialrequestTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
