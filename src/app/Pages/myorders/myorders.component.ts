import {
  Component,
  OnInit,
  ɵbypassSanitizationTrustResourceUrl,
} from '@angular/core';
import { MyOrderData } from '../../Models/myOrderData';
import { OrdersService } from 'src/app/Services/orders/orders.service';
import { TmplAstRecursiveVisitor } from '@angular/compiler';

@Component({
  selector: 'app-myorders',
  templateUrl: './myorders.component.html',
  styleUrls: ['./myorders.component.css'],
})
export class MyordersComponent implements OnInit {
  orders: Array<MyOrderData> = [];
  _loading: boolean = true;
  _loadingMessage = 'Loading your Orders';

  constructor(private ordersService: OrdersService) { }

  ngOnInit(): void {
    this.fetchMyOrders();
  }

  fetchMyOrders(): void {
    this.ordersService.getAllOrders().subscribe(
      (data: Array<MyOrderData>) => {
        data.forEach((order: any) => {
          order['food_items'].forEach((food) => {
            this.orders.push({
              food_name: food.food_name,
              comment: food.comment,
              food_id: food.food_id,
              quantity: food.quantity,
              price: food.food_price,
              orderDate: food.orderdate,
              status: food.status,
              orderId: order.order_id,
              food_image: food.food_image
            });
          });
        });
        this._loading = false;
      },
      (err) => console.log(err)
    );
  }

  // this is used to refresh the page when an order is cancelled, thus avoiding using windows.reload
  refreshPage(event: boolean): void {
    if (event) {
      this.orders = [];
      this.fetchMyOrders();
    }
  }
}
