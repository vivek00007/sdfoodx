import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { OrdersService } from 'src/app/Services/orders/orders.service';
import Swal from 'sweetalert2';
import { Router } from '@angular/router';

@Component({
  selector: 'app-ordercard',
  templateUrl: './ordercard.component.html',
  styleUrls: ['./ordercard.component.css']
})
export class OrdercardComponent implements OnInit {
  deleteList = [];
  constructor(private ordersService: OrdersService, private router: Router) { }
  @Input() foodname;
  @Input() quantity;
  @Input() price;
  @Input() comment;
  @Input() orderdate;
  @Input() foodid;
  @Input() status;
  @Input() orderid;
  @Input() foodimage;
  @Output() refreshMyOrders: EventEmitter<boolean> = new EventEmitter<any>();
  // emitter is used to refresh myorders page without reloading the page itself

  imageurl: string;

  ngOnInit(): void {
    if (this.imageurl != 'none') {
      this.imageurl = 'data:image/' + this.foodimage;
    }
  }

  // Replace it by delete order
  cancelOrder(
    id: number,
    fid: number,
    orderDate: string): void {
    const deleteList = [...this.deleteList,
    {
      order_id: id,
      food_id: fid,
      orderdate: orderDate.substr(0, 10),
    }
    ];
    console.log(JSON.stringify(deleteList));
    this.ordersService.deleteOrder(deleteList).subscribe(
      (data) => {
        Swal.fire('Success!', 'Order was cancelled', 'success');
        this.refreshMyOrders.emit(true);
      },
      (err) => console.log(err)
    );
  }

}
