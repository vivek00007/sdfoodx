import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManagefoodComponent } from './managefood.component';

describe('ManagefoodComponent', () => {
  let component: ManagefoodComponent;
  let fixture: ComponentFixture<ManagefoodComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManagefoodComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManagefoodComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
