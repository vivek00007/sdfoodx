import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { FoodData } from '../../Models/foodData';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import { FoodService } from 'src/app/Services/food/food.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Dropdown } from '../../Models/dropdown';
import { CategoryData } from '../../Models/category';
import { CategoriesService } from '../../Services/category/categories.service';
import Swal from 'sweetalert2';
import { forkJoin } from 'rxjs';

@Component({
  selector: 'app-managefood',
  templateUrl: './managefood.component.html',
  styleUrls: ['./managefood.component.css']
})
export class ManagefoodComponent implements OnInit {
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;

  public openModal = false;

  allCategories: Array<CategoryData>;

  displayedColumns: string[] = [
    'food_name',
    'food_description',
    'category_name',
    'food_price',
    'food_limit',
    'isVeg',
    'food_rating',
    'editimage',
    'edit',
    'delete'
  ];

  //Filters:
  search: string = '';
  selectedCategory: number;
  showClearButton = false;

  _loading: boolean = true;
  _loadingMessage = 'Loading food items';

  categoryOptions: Array<CategoryData> = [];

  allFood: Array<FoodData>;
  dataSource: MatTableDataSource<FoodData>;
  imageData: string | ArrayBuffer = null;

  // variables for inline editing of food data
  oldFoodRow = {} as FoodData;
  newFoodRow = {} as FoodData;
  editDisabled: boolean;
  newImageSet: boolean;

  constructor(
    private foodService: FoodService,
    private categoriesService: CategoriesService,
    private route: ActivatedRoute,
    private router: Router,
  ) { }

  ngOnInit(): void {
    const request = forkJoin([
      this.foodService.getAllFoods(),
      this.categoriesService.getAllCategories(),
    ]);
    request.subscribe(data => {
      this.allFood = data[0];
      this.dataSource = new MatTableDataSource(data[0]);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;

      this.categoryOptions = data[1];

      this._loading = false;
      this._loadingMessage = '';
      this.filterFood();
    }, err => console.log(err))
  }


  handleDropdownChange(e, type): void {
    this.showClearButton = true;
    this.selectedCategory = e.value;
    this.filterFood();
  }

  filterFood(): void {
    let filteredFood: FoodData[] = [...this.allFood];
    if (this.selectedCategory) {
      filteredFood = filteredFood.filter((b) => b.category_id === this.selectedCategory);
    }
    this.dataSource = new MatTableDataSource(filteredFood);
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  applyFilter(event: Event): void {
    const filterValue: string = (event.target as HTMLInputElement).value;
    this.search = filterValue;
    this.showClearButton = true;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  clearFilters(): void {
    this.search = '';
    this.selectedCategory = 0;
    this.showClearButton = false;
    this.dataSource = new MatTableDataSource(this.allFood);
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
    this.router.navigate(['/managefood']);
  }

  openAddFood(): void {
    this.openModal = true;
  }

  handleEdit(oldfood: any): void {
    console.log(oldfood);
    this.newFoodRow = oldfood && oldfood.food_id ? oldfood : {};
    this.oldFoodRow = { ...this.newFoodRow };
  }

  confirmEdit(): void {
    if (this.newFoodRow.food_name == ''
      || this.newFoodRow.food_description == ''
      || this.newFoodRow.food_price < 0
      || this.newFoodRow.food_limit < 0) {
      Swal.fire(
        'Incorrect Input!',
        'Please fill in all the fields correctly.',
        'warning'
      );
    }
    else {
      this.editDisabled = true;
      if (!this.newImageSet) {
        this.newFoodRow.food_image = this.oldFoodRow.food_image;
      }
      else {
        //substr to avoid sanitising url error
        this.newFoodRow.food_image = this.imageData.toString().substring(11, this.imageData.toString().length);
      }

      this.foodService.updateFood(this.newFoodRow)
        .subscribe((data: any) => {
          this.newFoodRow = {} as FoodData;
          this.editDisabled = false;
          this.newImageSet = false;
          this.clearFilters();
          Swal.fire(
            'Success!',
            'Details modified.',
            'success'
          );
        }, err => {
          Swal.fire(
            'Failure!',
            'Details were not modified.',
            'error'
          );
          this.editDisabled = false;
          this.cancelEdit();
        });
    }
  }

  cancelEdit(): void {
    this.newFoodRow = {} as FoodData;
    let index = this.allFood.findIndex(item => item.food_id === this.oldFoodRow.food_id);
    this.allFood.splice(index, 1, this.oldFoodRow);
    this.dataSource = new MatTableDataSource(this.allFood);
    this.editDisabled = false;
    this.clearFilters();
  }

  handleDelete(foodName: string): void {
    console.log(foodName);
    Swal.fire({
      title: 'Delete Food Item?',
      text: 'Are you sure?',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes',
    }).then((result) => {
      if (result.value) {
        let foodIndex: number = this.allFood.findIndex(f => f.food_name === foodName);
        let foodId: number = this.allFood[foodIndex].food_id;

        this.foodService.deleteFood(foodId).subscribe(
          (data) => {
            console.log(data);
            console.log('deleted');
            this.allFood.splice(foodIndex, 1);
            this.dataSource = new MatTableDataSource(this.allFood);
            this.clearFilters();
          },
          (err) => console.log(err)
        );
      }
    });
  }

  handleFileInput(files: FileList) {
    this.newImageSet = true;
    let me = this;
    let file = files[0];
    let reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = function () {
      console.log(reader.result);
      me.imageData = reader.result;
    };
    reader.onerror = function (error) {
      console.log('Error: ', error);
    };
  }
}
