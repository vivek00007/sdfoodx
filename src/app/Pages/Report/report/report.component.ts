import { Component, OnInit } from '@angular/core';
import { addDays } from '@progress/kendo-date-math';
import { addWeeks } from '@progress/kendo-date-math';
import { addMonths } from '@progress/kendo-date-math';
import { convertToCSV } from 'src/app/utils/generateCSV';
//import { ReportService } from '../../services/report/report.service';

const date = new Date();
var day = addDays(date, -1);

@Component({
  selector: 'app-report',
  templateUrl: './report.component.html',
  styleUrls: ['./report.component.css'],
})
export class ReportComponent {
  error = false;
  _loading: boolean = false;

  constructor() {}

  firstDay = new Date(); //returns first date of the month, `2000-11-1`
  weekly = (this.firstDay = addDays(date, -7));

  public range = { start: day, end: day };

  public changeWeek(x: any): void {
    this.range = { start: addWeeks(day, -x), end: day };
  }

  public changeMonth(y: any): void {
    this.range = { start: addMonths(day, -y), end: day };
  }

  generate_csv() {}
  min = new Date('1/1/2015');

  max = addMonths(date, 3);

  ngOnInit(): void {}
}
