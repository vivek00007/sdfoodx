import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import { OrdersService } from 'src/app/Services/orders/orders.service';
import { CategoriesService } from 'src/app/Services/category/categories.service'
import { ActivatedRoute, Router } from '@angular/router';
import { CategoryData } from '../../Models/category';
import Swal from 'sweetalert2';
import { AllOrderData } from '../../Models/allOrderData';
import { forkJoin } from 'rxjs';

@Component({
  selector: 'app-dailyorders',
  templateUrl: './dailyorders.component.html',
  styleUrls: ['./dailyorders.component.css']
})

export class DailyordersComponent implements OnInit {
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;

  displayedColumns: string[] = [
    'emp_name',
    'food_name',
    'quantity',
    'totalprice',
    'edit',
  ];

  //Filters:
  search: string = '';
  selectedCategory;
  showClearButton = false;

  _loading: boolean = true;
  _loadingMessage = 'Loading Daily Orders';

  categoryOptions: Array<CategoryData>;

  allOrders: Array<AllOrderData> = [];
  selectedCategoryFoodList: Array<AllOrderData>;

  dataSource: MatTableDataSource<AllOrderData>;


  constructor(
    private OrdersService: OrdersService,
    private route: ActivatedRoute,
    private router: Router,
    private CategoriesService: CategoriesService
  ) { }

  ngOnInit(): void {
    this.fetchOrdersAndCategories();
  }

  fetchOrdersAndCategories(): void {
    const request = forkJoin([
      this.CategoriesService.getAllCategories(),
      this.OrdersService.getAllOrders(),
    ]);

    request.subscribe(data => {
      this.categoryOptions = data[0];

      console.log(data[1]);
      data[1].forEach((order: any) => {
        order['food_items'].forEach((food) => {
          this.allOrders.push({
            emp_name: order.user_name,
            food_name: food.food_name,
            category_id: food.category_id,
            quantity: food.quantity,
            totalprice: (food.food_price * food.quantity),
            order_id: order.order_id,
            food_id: food.food_id,
            orderDate: food.orderdate,
            status: food.status,
            comment: food.comment
          });
        });
      });

      // filter all Orders by Today's date and where status is approved 
      this.allOrders = this.allOrders.filter(
        (b) => (b.status === 1 || b.status === 3)
          && ((new Date(b.orderDate).getTime() - new Date().getTime()) <= 86400)
      );

      console.log(this.allOrders);
      this.dataSource = new MatTableDataSource(this.allOrders);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
      this._loading = false;
      this.filterFood();
    }, err => console.log(err));
  }

  handleDropdownChange(): void {
    this.showClearButton = true;
    this.selectedCategory == 0 ? this.selectedCategoryFoodList = this.allOrders :
      this.selectedCategoryFoodList = this.allOrders.filter((b) => b.category_id == this.selectedCategory);
    this.dataSource = new MatTableDataSource(this.selectedCategoryFoodList);
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  filterFood(): void {
    let filteredFood: AllOrderData[] = [...this.allOrders];
    if (this.selectedCategory) {
      filteredFood = filteredFood.filter((b) => b.category_id === this.selectedCategory);
    }
    this.dataSource = new MatTableDataSource(filteredFood);
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  applyFilter(event: Event): void {
    const filterValue: string = (event.target as HTMLInputElement).value;
    this.search = filterValue;
    this.showClearButton = true;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  clearFilters(): void {
    this.search = '';
    this.selectedCategory = '';
    this.showClearButton = false;
    this.dataSource = new MatTableDataSource(this.allOrders);
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
    this.router.navigate(['/dailyorders']);
  }

  handleEdit(
    id: number,
    fid: number,
    orderDate: string,
    status: number): void {
    const body = {
      status,
      order_id: id,
      food_id: fid,
      orderdate: orderDate,
    };
    console.log(body);
    this.OrdersService.changeStatus(body).subscribe(
      (data) => {
        Swal.fire('Order Completed!', 'Employee purchased the food', 'success');
        this.allOrders = [];
        this.fetchOrdersAndCategories();
        // this.dataSource = new MatTableDataSource(this.allFood);
        // this.dataSource.paginator = this.paginator;
        // this.dataSource.sort = this.sort;
      },
      (err) => console.log(err)
    );

  }

}
