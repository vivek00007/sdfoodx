import { Component, OnInit } from '@angular/core';
import { CategoryData } from '../../Models/category';
import { ActivatedRoute, Router } from '@angular/router';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { CategoriesService } from '../../Services/category/categories.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-managecategory',
  templateUrl: './managecategory.component.html',
  styleUrls: ['./managecategory.component.css']
})
export class ManagecategoryComponent implements OnInit {

  allCategories: Array<CategoryData>;

  constructor(
    private categoriesService: CategoriesService,
    private route: ActivatedRoute,
    private router: Router,
  ) { }

  _loading: boolean = true;
  _loadingMessage = 'Loading Categories';
  addCategoryForm: FormGroup;
  ngOnInit(): void {
    this.fetchCategory();
    this.addCategoryForm = new FormGroup({
      categoryname: new FormControl('', [Validators.required]),
    });
  }

  fetchCategory(): void {
    this.categoriesService.getAllCategories().subscribe(
      (data: Array<CategoryData>) => {
        this.allCategories = data;
        console.log(this.allCategories);
      },
      (err) => console.log(err)
    );
  }

  handleDelete(foodName: string): void {
    console.log(foodName);
    Swal.fire({
      title: 'Delete Category?',
      text: 'Are you sure?',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes',
    }).then((result) => {
      if (result.value) {
        let foodIndex: number = this.allCategories.findIndex(f => f.category_name === foodName);
        let foodId: number = this.allCategories[foodIndex].category_id;

        this.categoriesService.deleteCategory(foodId).subscribe(
          (data) => {
            console.log(data);
            console.log('deleted');
            this.allCategories.splice(foodIndex, 1);
            this.router.navigateByUrl('/managefood');
          },
          (err) => console.log(err)
        );
      }
    });
  }

  createCategory(): void {
    let jsonBody = {
      category_name: this.addCategoryForm.value.categoryname,
    };
    console.log(this.addCategoryForm.value.categoryname);
    this.categoriesService.addCategory(jsonBody).subscribe(
      (data) => {
        Swal.fire('Success!', 'You have created a new category.', 'success').then(
          () => {
            //this.router.navigateByUrl('/managefood');
            this.fetchCategory();
          }
        );

        console.log('data sent');
      },
      (err) => {
        console.log(err);
      }
    );
  }

  get categoryname(): FormControl {
    return this.addCategoryForm.get('categoryname') as FormControl;
  }

}
