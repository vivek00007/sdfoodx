import { Component, OnInit, ViewChild, Inject } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import { OrdersService } from 'src/app/Services/orders/orders.service';
import { ActivatedRoute, Router } from '@angular/router';
import Swal from 'sweetalert2';
import { AllOrderData } from '../../Models/allOrderData';
import { CategoryData } from 'src/app/Models/category';
import { CategoriesService } from 'src/app/Services/category/categories.service';
import { forkJoin } from 'rxjs';

@Component({
  selector: 'app-managefood',
  templateUrl: './manageorders.component.html',
  styleUrls: ['./manageorders.component.css'],
})
export class ManageordersComponent implements OnInit {
  foodOrderComment: string = '';
  displayedColumns: string[] = [
    'emp_name',
    'food_name',
    'Quantity',
    'orderDate',
    'comment',
    'edit_comment',
    'actions',
  ];

  _loading: boolean = true;
  _loadingMessage = 'Loading All Orders';
  showClearButton = false;
  orders = [];
  // FILTERS
  search: string = '';
  selectedCategory;

  // variables for inline editing of food data
  oldOrderRow = {} as AllOrderData;
  newOrderRow = {} as AllOrderData;
  editDisabled: boolean;

  categoryOptions: Array<CategoryData>;
  selectedCategoryOrderList: Array<AllOrderData>;
  dataSource: MatTableDataSource<AllOrderData>;
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;

  constructor(
    private ordersService: OrdersService,
    private router: Router,
    private categoriesService: CategoriesService) { }

  ngOnInit(): void {
    this.fetchOrdersAndCategories();
  }

  fetchOrdersAndCategories(): void {
    const request = forkJoin([
      this.categoriesService.getAllCategories(),
      this.ordersService.getAllOrders(),
    ]);

    request.subscribe(data => {
      this.categoryOptions = data[0];

      data[1].forEach((order: any) => {
        order['food_items'].forEach((food) => {
          this.orders.push({
            category_id: food.category_id,
            emp_name: order.user_name,
            food_name: food.food_name,
            Quantity: food.quantity,
            comment: food.comment,
            order_id: order.order_id,
            food_id: food.food_id,
            status: food.status,
            orderDate: food.orderdate,
          });
        });
      });

      this.dataSource = new MatTableDataSource(this.orders);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
      this._loading = false;
      this.filterFood();

    }, err => console.log(err));
  }

  filterFood(): void {
    let filteredorders: AllOrderData[] = [...this.orders];
    if (this.selectedCategory) {
      filteredorders = filteredorders.filter((b) => b.category_id === this.selectedCategory);
    }
    this.dataSource = new MatTableDataSource(filteredorders);
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  applyFilter(event: Event): void {
    const filterValue: string = (event.target as HTMLInputElement).value;
    this.search = filterValue;
    this.showClearButton = true;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  clearFilters(): void {
    this.search = '';
    this.selectedCategory = '';
    this.showClearButton = false;
    this.dataSource = new MatTableDataSource(this.orders);
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
    this.router.navigate(['/manageorders']);
  }

  handleDropdownChange(): void {
    this.showClearButton = true;
    this.selectedCategory == 0 ? this.selectedCategoryOrderList = this.orders :
      this.selectedCategoryOrderList = this.orders.filter((b) => b.category_id == this.selectedCategory);
    this.dataSource = new MatTableDataSource(this.selectedCategoryOrderList);
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  handleEdit(oldOrder: any): void {
    console.log(oldOrder);
    this.newOrderRow = oldOrder && oldOrder.food_id ? oldOrder : {};
    this.oldOrderRow = { ...this.newOrderRow };
  }

  changeStatus(
    id: number,
    fid: number,
    orderDate: string,
    status: number
  ): void {
    const body = {
      status,
      order_id: id,
      food_id: fid,
      orderdate: orderDate,
    };
    console.log(body);
    this.ordersService.changeStatus(body).subscribe(
      (data) => {
        Swal.fire('Success!', 'Status changed successfully', 'success');
        this.orders = [];
        this.clearFilters();
        this.fetchOrdersAndCategories();
        this.dataSource = new MatTableDataSource(this.orders);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      },
      (err) => console.log(err)
    );
  }

  confirmEdit(): void {
    this.editDisabled = true;
    this.ordersService.updateOrder(this.newOrderRow).subscribe(
      (data: any) => {
        this.newOrderRow = {} as AllOrderData;
        this.editDisabled = false;
        Swal.fire('Success!', 'Comment modified.', 'success');
        this.clearFilters();
      },
      (err) => {
        Swal.fire('Failure!', 'Details were not modified.', 'error');
        this.editDisabled = false;
      }
    );
  }
}
