import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SchedulefoodComponent } from './schedulefood.component';

describe('SchedulefoodComponent', () => {
  let component: SchedulefoodComponent;
  let fixture: ComponentFixture<SchedulefoodComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SchedulefoodComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SchedulefoodComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
