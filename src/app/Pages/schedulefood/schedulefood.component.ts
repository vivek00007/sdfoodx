import { Component, OnInit } from '@angular/core';
import { CdkDragDrop, moveItemInArray, transferArrayItem, copyArrayItem } from '@angular/cdk/drag-drop';
import { Board } from 'src/app/models/board.model';
import { Column } from 'src/app/models/column.model';
import { Schedule } from 'src/app/models/Schedule.model';
import { HttpClient } from '@angular/common/http';
import { FoodService } from 'src/app/Services/food/food.service';
import { SchedulesService } from 'src/app/Services/schedules/schedules.service';
import { forkJoin } from 'rxjs';
import { elementEventFullName } from '@angular/compiler/src/view_compiler/view_compiler';
import { FoodItem } from 'src/app/models/food_item';
import { identifierModuleUrl } from '@angular/compiler';
import Swal from 'sweetalert2';
import { Router } from '@angular/router';

@Component({
  selector: 'app-schedulefood',
  templateUrl: './schedulefood.component.html',
  styleUrls: ['./schedulefood.component.css', './schedulefood.scss']
})

export class SchedulefoodComponent implements OnInit {
  daysName: string[] = ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Everyday'];
  colArr: Column[] = [];
  all: Column[] = [];
  fixed: Column[] = [];
  connect: any = [];

  deleted: Schedule[] = [];
  added: Schedule[] = [];

  _loading: boolean = true;
  _loadingMessage = 'Loading Schedules';

  deleteCard(colName, index: number, food: FoodItem, type: string): void {
    if (type == "delete") {
      this.deleted.push({ food_id: food.food_id, day: this.daysName.indexOf(colName.name) })
    }

    if (type == "remove") {
      let i = this.added.findIndex(a => a.food_id == food.food_id && a.day == this.daysName.indexOf(colName.name))
      this.added.splice(i, 1)
    }
    colName.tasks.splice(index, 1);
  }

  returnhome(this: CdkDragDrop<string[]>) {
    alert(this.container);
  }

  constructor(
    private http: HttpClient,
    private foodservice: FoodService,
    private scheduleService: SchedulesService,
    private router: Router) {
    this.daysName.forEach(d => this.colArr.push(new Column(d, [])));
  }

  ngOnInit(): void {
    this.fetchSchedules();
  }

  fetchSchedules(): void {
    const requests = forkJoin([this.foodservice.getAllFoods(), this.scheduleService.getAllSchedules()]);

    requests.subscribe(data => {
      this.all = [...this.colArr];
      this.fixed.push(new Column('All Food', data[0].map(a => new FoodItem(a.food_id, a.food_name))));

      data[1].forEach(s =>
        this.fixed.forEach(el =>
          el.tasks.forEach(element => {
            const myFoodItem: FoodItem = new FoodItem(element.food_id, element.food_name, element.type = "delete");
            (element.food_id == s.food_id) ? this.all[s.day_id].tasks.push(myFoodItem) : null;
          }
          )));
      console.log(this.fixed[0]);
      console.log(this.all);

      this.connect = ['All Food', ...this.daysName];
      this._loading = false;
    });
  }

  drop(event: CdkDragDrop<string[]>) {
    event.previousContainer.data[event.previousIndex]["type"] = "remove";

    if (event.previousContainer === event.container) {
      moveItemInArray(event.container.data, event.previousIndex, event.currentIndex);
    }
    else {
      if (event.previousContainer.id !== "All Food") {
        if (event.container.id === "All Food") {
          const food: any = event.previousContainer.data[event.previousIndex];
          let i = this.added.findIndex(a => a.food_id == food.food_id && a.day == this.daysName.indexOf(event.previousContainer.id))
          this.added.splice(i, 1)
          this.deleted.push({ food_id: food.food_id, day: this.daysName.indexOf(event.previousContainer.id) })

          transferArrayItem(event.previousContainer.data,
            event.container.data,
            event.previousIndex,
            event.currentIndex);
          event.container.data.splice(event.currentIndex, 1);
        }
        else {
          if (!event.container.data.includes(event.previousContainer.data[event.previousIndex])) {
            const food: any = event.previousContainer.data[event.previousIndex];
            let idx = event.container.data.findIndex(a => a["food_id"] == food.food_id)

            if (idx == -1) {
              this.added.push({ food_id: food.food_id, day: this.daysName.indexOf(event.container.id) })

              let i = this.added.findIndex(a => a.food_id == food.food_id && a.day == this.daysName.indexOf(event.previousContainer.id))
              this.added.splice(i, 1)
              this.deleted.push({ food_id: food.food_id, day: this.daysName.indexOf(event.previousContainer.id) })
              this.added.push({ food_id: food.food_id, day: this.daysName.indexOf(event.container.id) })
              transferArrayItem(event.previousContainer.data,
                event.container.data,
                event.previousIndex,
                event.currentIndex);
            }
          }
        }
      }
      else {
        const food: any = event.previousContainer.data[event.previousIndex];
        let idx = event.container.data.findIndex(a => a["food_id"] == food.food_id)

        if (idx == -1) {
          const food: any = event.previousContainer.data[event.previousIndex];
          this.added.push({ food_id: food.food_id, day: this.daysName.indexOf(event.container.id) })
          copyArrayItem(event.previousContainer.data,
            event.container.data,
            event.previousIndex,
            event.currentIndex);
        }
      }
    }
  }

  save(): void {
    Swal.fire({
      title: 'Are you sure?',
      text: 'You won\'t be able to revert this!',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, save it!'
    }).then((result) => {
      if (result.value) {
        if (this.deleted.length > 0) {
          this.scheduleService.deleteSchedules(this.deleted).subscribe(res => {
            console.log(res);
            window.location.reload();
          }, err => console.log(err));
        }
        if (this.added.length > 0) {
          this.scheduleService.addSchedules(this.added).subscribe(data => {
            window.location.reload();
            this.router.navigateByUrl('/managefood');
            // //this.colArr = [];
            // this.all = [];
            // this.fixed = [];
            // this.connect = [];
            // this.deleted = [];
            // this.added = [];
            // this.fetchSchedules();
          }, err => {
            console.log(err);
          });
        }

        Swal.fire(
          'Save!',
          'Your changes has been saved.',
          'success'
        );
      }
    });
    console.log(this.added);
    console.log(this.deleted);

  }
}
