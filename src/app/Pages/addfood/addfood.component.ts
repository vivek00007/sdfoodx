import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { CategoryData } from '../../Models/category';
import { CategoriesService } from '../../Services/category/categories.service';
import { FoodService } from 'src/app/Services/food/food.service';
import { Router } from '@angular/router';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-addfood',
  templateUrl: './addfood.component.html',
  styleUrls: ['./addfood.component.css'],
})
export class AddfoodComponent implements OnInit {
  constructor(
    private categoriesService: CategoriesService,
    private foodService: FoodService,
    private router: Router
  ) { }
  vegfield: boolean;
  addFoodForm: FormGroup;
  allCategories: Array<CategoryData>;

  _loading: boolean = true;
  _loadingMessage = 'Loading food items';
  imageData: string | ArrayBuffer = null;

  ngOnInit(): void {
    this.fetchCategory();
    this.addFoodForm = new FormGroup({
      foodname: new FormControl('', [Validators.required]),
      description: new FormControl('', [Validators.required]),
      category: new FormControl('', [Validators.required]),
      price: new FormControl('', [Validators.required]),
      limit: new FormControl('', [Validators.required]),
      isVeg: new FormControl('', [Validators.required]),
    });
  }

  fetchCategory(): void {
    this.categoriesService.getAllCategories().subscribe(
      (data: Array<CategoryData>) => {
        this.allCategories = data;
        console.log(this.allCategories);
      },
      (err) => console.log(err)
    );
  }

  createFood(): void {
    // matching the radio buttons to their corresponding boolean field
    this.addFoodForm.value.isVeg == 1
      ? (this.vegfield = true)
      : (this.vegfield = false);

    // i separated the data:image part to avoid sanitising url issues
    let imageurl: string;
    if (this.imageData != '' || this.imageData != null) {
      imageurl = this.imageData.toString().substring(11, this.imageData.toString().length);
    }
    else {
      imageurl = 'none';
    }
    let jsonBody = {
      food_name: this.addFoodForm.value.foodname,
      food_description: this.addFoodForm.value.description,
      food_price: this.addFoodForm.value.price,
      food_image: imageurl,
      food_rating: 0,
      food_limit: this.addFoodForm.value.limit,
      isVeg: this.vegfield,
      category_id: this.addFoodForm.value.category,
    };

    this.foodService.addFood(jsonBody).subscribe(
      (data) => {
        Swal.fire('Success!', 'You have uploaded a new food.', 'success').then(
          () => {
            this.router.navigateByUrl('/schedulefood');
          }
        );
        console.log('data sent');
      },
      (err) => {
        console.log(err);
      }
    );
  }

  handleCategory(e) {
    this.category.setValue(e.target.value, {
      onlySelf: true,
    });
  }

  // these fucntions get the data in each of the form control initialised above
  // in each div there is a formcontrol class which has each of the names below
  get foodname(): FormControl {
    return this.addFoodForm.get('foodname') as FormControl;
  }
  get description(): FormControl {
    return this.addFoodForm.get('description') as FormControl;
  }
  get category(): FormControl {
    return this.addFoodForm.get('category') as FormControl;
  }
  get price(): FormControl {
    return this.addFoodForm.get('price') as FormControl;
  }
  get limit(): FormControl {
    return this.addFoodForm.get('limit') as FormControl;
  }
  get isVeg(): FormControl {
    return this.addFoodForm.get('isVeg') as FormControl;
  }

  handleFileInput(files: FileList) {
    let me = this;
    let file = files[0];
    let reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = function () {
      console.log(reader.result);
      me.imageData = reader.result;
    };
    reader.onerror = function (error) {
      console.log('Error: ', error);
    };
  }
}
