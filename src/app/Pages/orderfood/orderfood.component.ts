import { Component, OnInit } from '@angular/core';
import { FoodData } from '../../Models/foodData';
import { ActivatedRoute, Router } from '@angular/router';
import { DateInputsModule } from '@progress/kendo-angular-dateinputs';
import { CategoryData } from '../../Models/category';
import { CategoriesService } from '../../Services/category/categories.service';
import { SchedulesService } from '../../Services/schedules/schedules.service';
import { SelectedFood } from '../../Models/selectedFood';
import { forkJoin } from 'rxjs';
import { addDays } from '@progress/kendo-date-math';

@Component({
  selector: 'app-orderfood',
  templateUrl: './orderfood.component.html',
  styleUrls: ['./orderfood.component.css'],
})
export class OrderfoodComponent implements OnInit {
  daysList = [
    'Sunday',
    'Monday',
    'Tuesday',
    'Wednesday',
    'Thursday',
    'Friday',
    'Saturday',
    'Everyday',
  ];

  allFood: Array<FoodData>;
  selectedCategoryFoodList: Array<FoodData>;
  allCategories: Array<CategoryData>;
  selectedFoodList: Array<SelectedFood> = [];

  _loading: boolean = true;
  _loadingMessage = 'Loading Menu';
  selectedCategory;
  selectedDate: Date = new Date();
  totalPrice: number = 0;
  selectedDay: string;
  public value: Date = new Date();
  public format: string = 'dd/MM/yyyy ';
  public MinDate = new Date();
  public MaxDate = addDays(new Date(), 60);

  constructor(
    private categoriesService: CategoriesService,
    private scheduleService: SchedulesService,
    private route: ActivatedRoute,
    private router: Router,
  ) { }

  ngOnInit(): void {
    this.fetchFoodsAndCategories();
  }

  fetchFoodsAndCategories(): void {
    const request = forkJoin([
      this.scheduleService.getAllSchedules(),
      this.categoriesService.getAllCategories(),
    ]);
    request.subscribe(data => {
      console.log(data[0]);
      this.allFood = data[0].filter(
        (b) => b.day == 'Everyday' || b.day == this.getWeekDay()
      );

      this.allFood = this.allFood.filter(
        (food, i, arr) =>
          arr.findIndex((t) => t.food_id === food.food_id) === i
      );

      this.selectedCategoryFoodList = this.allFood;
      this._loading = false;
      this._loadingMessage = '';

      this.allCategories = data[1];
      console.log(this.allCategories);
    }, (err) => console.log(err))
  }

  handleCategory(): void {
    console.log(this.selectedCategory);
    this.selectedCategory == 0
      ? (this.selectedCategoryFoodList = this.allFood)
      : (this.selectedCategoryFoodList = this.allFood.filter(
        (b) => b.category_id == this.selectedCategory
      ));
  }

  handleDateChange(event): void {
    this.selectedDate = event;
    this.allFood = [];
    this.selectedCategoryFoodList = [];
    this.fetchFoodsAndCategories();
  }

  getWeekDay(): string {
    let d = this.selectedDate.getDay();
    this.selectedDay = this.daysList[d];
    console.log(this.daysList[d]);
    return this.daysList[d];
  }

  filterAllFoodByDate(): void { }

  addToCart(event: SelectedFood): void {
    let formattedDate = this.selectedDate.toISOString().substr(0, 10);
    let index: number = this.selectedFoodList.findIndex(
      (e) => e.foodId == event.foodId && e.orderDate == formattedDate
    );
    if (index != -1) {
      this.selectedFoodList.splice(index, 1);
    }
    let position = this.selectedFoodList.push(event);
    console.log(position);
    this.selectedFoodList[position - 1].orderDate = formattedDate;
    console.log(this.selectedFoodList);
    this.totalPrice = this.selectedFoodList.reduce(
      (acc, food) => acc + food.totalPrice,
      0
    );
  }
}

