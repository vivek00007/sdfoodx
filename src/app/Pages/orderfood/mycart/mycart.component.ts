import { Component, OnInit, Input } from '@angular/core';
import { SelectedFood } from '../../../Models/selectedFood';
import Swal from 'sweetalert2';
import { OrdersService } from '../../../Services/orders/orders.service';
import { Router } from '@angular/router';
@Component({
  selector: 'app-mycart',
  templateUrl: './mycart.component.html',
  styleUrls: ['./mycart.component.css']
})
export class MycartComponent implements OnInit {
  selectedFoodList: Array<SelectedFood> = [];
  orders = [];

  constructor(private orderService: OrdersService, private router: Router) { }
  @Input() foodList: Array<SelectedFood>;

  ngOnInit(): void {
    this.selectedFoodList = this.foodList;
    console.log(this.selectedFoodList);
  }

  confirmOrder(): void {
    console.log(this.foodList);
    // no validation
    this.selectedFoodList.forEach(food => {
      this.orders = [
        ...this.orders,
        {
          quantity: food.quantityBought,
          orderdate: food.orderDate,
          food_id: food.foodId
        }
      ];
    });

    Swal.fire({
      title: 'Confirm Orders?',
      text: "Are you done ordering?",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes',
    }).then((result) => {
      if (result.value) {
        console.log(this.orders);
        this.orderService.addOrders(this.orders).subscribe(
          (data) => {
            Swal.fire(
              'Success!',
              'Your orders have been made.',
              'success'
            ).then(() => {
              this.router.navigateByUrl('/myorders');
            });
          },
          (err) => {
            console.log(err);
          }
        );
      }
    });
  }

  handleDelete(deletedFood: SelectedFood): void {
    this.selectedFoodList.splice(this.selectedFoodList.findIndex((b) => b == deletedFood), 1);
  }
}
