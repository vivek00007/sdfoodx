import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { SelectedFood } from '../../../Models/selectedFood';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-foodcard',
  templateUrl: './foodcard.component.html',
  styleUrls: ['./foodcard.component.css']
})
export class FoodcardComponent implements OnInit {
  quantity: number;

  constructor() { }
  @Input() description;
  @Input() name;
  @Input() foodId;
  @Input() price;
  @Input() limit;
  @Input() isVeg;
  @Input() foodimage;
  @Output() cartFood: EventEmitter<any> = new EventEmitter<any>();
  url: string;
  ngOnInit(): void {
    this.url = 'data:image/' + this.foodimage; // i separated the data:image part to avoid sanitising url issues
  }

  emitTotalPrice(totalPrice: number, foodId: number, name: string, quantity: number) {
    if (totalPrice > 0) {
      const newSelectedFood: SelectedFood = { totalPrice, quantityBought: quantity, foodId, foodName: name, orderDate: '' };
      console.log(newSelectedFood);
      this.cartFood.emit(newSelectedFood);
      Swal.fire({
        position: 'center',
        icon: 'success',
        title: 'Meal added to cart!',
        showConfirmButton: false,
        timer: 1500
      }
      )
    }
  }

}

