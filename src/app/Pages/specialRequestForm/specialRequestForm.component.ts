import { Component, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { FormControl, Validators } from '@angular/forms';
import { DateInputsModule } from '@progress/kendo-angular-dateinputs';
import { SpecialService } from 'src/app/Services/specialRequest/special.service';
import Swal from 'sweetalert2';
import { addDays } from '@progress/kendo-date-math';
import { Router } from '@angular/router';

@Component({
  selector: 'app-specialRequestForm',
  templateUrl: './specialRequestForm.component.html',
  styleUrls: ['./specialRequestForm.component.css'],
})
export class SpecialRequestFormComponent implements OnInit {
  public value: Date = addDays(new Date(), 1);
  public format: string = 'MM/dd/yyyy ';
  public _error = false;
  public min = addDays(new Date(), 1);
  public max = addDays(new Date(), 61);
  FoodForm: FormGroup;

  constructor(private SpecialService: SpecialService, private router: Router) { }

  ngOnInit() {
    this.FoodForm = new FormGroup({
      description: new FormControl(),
      comment: new FormControl(),
    });
  }
  get description() {
    return this.FoodForm.get('description');
  }
  get comment() {
    return this.FoodForm.get('comment');
  }

  submit(): void {

    if (this.FoodForm.valid) {
      const description = this.FoodForm.value.description;
      const comment = this.FoodForm.value.comment;
      console.log(description, comment, this.value);
      this.SpecialService.submitRequest(
        description,
        comment,
        this.value
      ).subscribe(
        (data) => {
          Swal.fire('Success!', 'Special request made.', 'success');
          setTimeout(() => {
            this.router.navigate(['/myspecialrequest']);
          }, 1500);
        },
        (err) => {
          this._error = true;
          Swal.fire('Oops...', 'An error has occured, retry!', 'error');
        }
      );
    }

  }
}
