import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/Services/auth/auth.service';
import { StorageService } from 'src/app/Services/storage/storage.service';

@Component({
  selector: 'app-navigationbar',
  templateUrl: './navigationbar.component.html',
  styleUrls: ['./navigationbar.component.css']
})
export class NavigationbarComponent implements OnInit {
  _role;
  username;

  constructor(
    private storageService: StorageService,
    private authService: AuthService
  ) {
    this._role = this.storageService.getCookie('role');
    this.username = this.storageService.getCookie('name');
  }

  ngOnInit(): void {
  }

  logout(): void {
    this.authService.logout();
  }

}
